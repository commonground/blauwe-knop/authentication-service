// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package authenticationservice

import (
	"errors"
	"fmt"
	"log"
)

var ErrDebtRequestIdEmpty = errors.New("request id is required")
var ErrOINEmpty = errors.New("oin is required")
var ErrSessionTokenEmpty = errors.New("sessionToken is required")
var ErrSessionTokenInvalid = errors.New("sessionToken is invalid")
var ErrUnableToFetchDebtRequest = errors.New("unable to fetch the debt request")
var ErrOINNotPresentInSchuldenRequest = errors.New("the oin of the source organization is not present in the schulden request")
var ErrFailedToAddLinkTokenToRequestID = errors.New("failed to add link token to request id")
var ErrFailedToAddBSNTOLinkToken = errors.New("failed to add bsn to link token")

type LinkTokenRepository interface {
	GetLinkToken(requestID string) (*LinkToken, error)
	CreateLinkToken(requestID string) (*LinkToken, error)
	AddBSNToLinkToken(bsn string, linkToken *LinkToken) error
}

type SessionRegisterRepository interface {
	GetSession(sessionToken string) (*SessionToken, error)
}

type DebtRequestRepository interface {
	Get(organization *Organization, id string) (*SchuldenRequest, error)
}

type SchemeRepository interface {
	GetOrganizationByOIN(OIN string) (*Organization, error)
}

type LinkTokenUseCase struct {
	organizationOIN           string
	linkTokenRepository       LinkTokenRepository
	debtRequestRepository     DebtRequestRepository
	schemeRepository          SchemeRepository
	sessionRegisterRepository SessionRegisterRepository
}

func NewLinkTokenUseCase(organizationOIN string, linkTokenRepository LinkTokenRepository, debtRequestRepository DebtRequestRepository, schemeRepository SchemeRepository, sessionRegisterRepository SessionRegisterRepository) *LinkTokenUseCase {
	return &LinkTokenUseCase{
		organizationOIN,
		linkTokenRepository,
		debtRequestRepository,
		schemeRepository,
		sessionRegisterRepository,
	}
}

func (a *LinkTokenUseCase) RequestLinkToken(oin string, debtRequestId string, sessionToken string) (linkToken LinkToken, error error) {
	if debtRequestId == "" {
		return "", ErrDebtRequestIdEmpty
	}

	if oin == "" {
		return "", ErrOINEmpty
	}

	if sessionToken == "" {
		return "", ErrSessionTokenEmpty
	}

	sessionTokenResponse, err := a.sessionRegisterRepository.GetSession(sessionToken)
	if err != nil {
		return "", fmt.Errorf("unable to check if session token exists: %s", err)
	}

	if sessionTokenResponse == nil {
		return "", ErrSessionTokenInvalid
	}

	existingLinkToken, err := a.linkTokenRepository.GetLinkToken(debtRequestId)
	if err != nil {
		return "", fmt.Errorf("unable to get link token: %s", err)
	}

	if existingLinkToken != nil {
		return *existingLinkToken, nil
	}

	organization, err := a.schemeRepository.GetOrganizationByOIN(oin)
	if err != nil {
		return "", fmt.Errorf("failed to fetch organization for oin: %s", err)
	}

	debtRequest, err := a.debtRequestRepository.Get(organization, debtRequestId)
	if err != nil {
		log.Printf("failed to get debt request: %s", err)
		return "", ErrUnableToFetchDebtRequest
	}

	if !isOINPresentInSchuldenRequest(a.organizationOIN, debtRequest) {
		return "", ErrOINNotPresentInSchuldenRequest
	}

	createdLinkToken, err := a.linkTokenRepository.CreateLinkToken(debtRequestId)
	if err != nil {
		log.Printf("failed to create link token: %s", err)
		return "", ErrFailedToAddLinkTokenToRequestID
	}

	err = a.linkTokenRepository.AddBSNToLinkToken(debtRequest.BSN, createdLinkToken)
	if err != nil {
		log.Printf("failed to add bsn to link token: %s", err)
		return "", ErrFailedToAddBSNTOLinkToken
	}

	return *createdLinkToken, nil
}

func isOINPresentInSchuldenRequest(oin string, request *SchuldenRequest) bool {
	var isOINPresentInSchuldenRequest bool
	for _, organization := range request.Organizations {
		if organization.OIN == oin {
			isOINPresentInSchuldenRequest = true
		}
	}

	return isOINPresentInSchuldenRequest
}
