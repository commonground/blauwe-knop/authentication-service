// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package authenticationservice_test

import (
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"authenticationservice"
	"authenticationservice/mock"
)

func TestAuthTokenUseCase_AddBSNToAuthtoken(t *testing.T) {
	type fields struct {
		authTokenRepository authenticationservice.AuthTokenRepository
	}
	type args struct {
		authToken string
		bsn       string
	}
	tests := []struct {
		name          string
		fields        fields
		args          args
		expectedError error
	}{
		{
			name: "authtoken is empty",
			fields: fields{
				authTokenRepository: func() authenticationservice.AuthTokenRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockAuthTokenRepository(ctrl)
					return repo
				}(),
			},
			args: args{
				bsn:       "814859094",
				authToken: "",
			},
			expectedError: authenticationservice.ErrAuthtokenEmpty,
		},
		{
			name: "bsn is empty",
			fields: fields{
				authTokenRepository: func() authenticationservice.AuthTokenRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockAuthTokenRepository(ctrl)
					return repo
				}(),
			},
			args: args{
				bsn:       "",
				authToken: "authtoken",
			},

			expectedError: authenticationservice.ErrBSNEmpty,
		},
		{
			name: "bsn invalid",
			fields: fields{
				authTokenRepository: func() authenticationservice.AuthTokenRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockAuthTokenRepository(ctrl)
					return repo
				}(),
			},
			args: args{
				bsn:       "814859094-invalid",
				authToken: "authtoken",
			},
			expectedError: authenticationservice.ErrBSNInvalid,
		},
		{
			name: "authtoken does not exist",
			fields: fields{
				authTokenRepository: func() authenticationservice.AuthTokenRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockAuthTokenRepository(ctrl)
					repo.EXPECT().DoesAuthTokenExist("authtoken").Return(false, nil).AnyTimes()
					return repo
				}(),
			},
			args: args{
				bsn:       "814859094",
				authToken: "authtoken",
			},
			expectedError: authenticationservice.ErrAuthTokenDoesNotExist,
		},
		{
			name: "error getting authtoken from repository",
			fields: fields{
				authTokenRepository: func() authenticationservice.AuthTokenRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					repo := mock.NewMockAuthTokenRepository(ctrl)
					repo.EXPECT().DoesAuthTokenExist("authtoken").Return(false, errors.New("error")).AnyTimes()
					return repo
				}(),
			},
			args: args{
				bsn:       "814859094",
				authToken: "authtoken",
			},
			expectedError: errors.New("error"),
		},
		{
			name: "bsn could not be added to token",
			fields: fields{
				authTokenRepository: func() authenticationservice.AuthTokenRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					bsn, _ := authenticationservice.NewBSN("814859094")

					repo := mock.NewMockAuthTokenRepository(ctrl)
					repo.EXPECT().DoesAuthTokenExist("authtoken").Return(true, nil).AnyTimes()
					repo.EXPECT().AddBSNToAuthToken("authtoken", bsn).Return(errors.New("error adding bsn")).AnyTimes()
					return repo
				}(),
			},
			args: args{
				bsn:       "814859094",
				authToken: "authtoken",
			},
			expectedError: errors.New("error adding bsn"),
		},
		{
			name: "bsn successfully added to token",
			fields: fields{
				authTokenRepository: func() authenticationservice.AuthTokenRepository {
					ctrl := gomock.NewController(t)
					defer ctrl.Finish()

					bsn, _ := authenticationservice.NewBSN("814859094")

					repo := mock.NewMockAuthTokenRepository(ctrl)
					repo.EXPECT().DoesAuthTokenExist("authtoken").Return(true, nil).AnyTimes()
					repo.EXPECT().AddBSNToAuthToken("authtoken", bsn).Return(nil).AnyTimes()
					return repo
				}(),
			},
			args: args{
				bsn:       "814859094",
				authToken: "authtoken",
			},
			expectedError: nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			authTokenUserCase := authenticationservice.NewAuthTokenUseCase("demo-org", "", tt.fields.authTokenRepository)
			err := authTokenUserCase.AddBSNToAuthtoken(tt.args.authToken, tt.args.bsn)
			assert.Equal(t, tt.expectedError, err)
		})
	}
}

func TestAuthTokenUseCase_Organization(t *testing.T) {
	authUseCase := authenticationservice.NewAuthTokenUseCase("mock-org", "", nil)
	assert.Equal(t, "mock-org", authUseCase.Organization())
}

func TestAuthTokenUseCase_OrganizationLogoURL(t *testing.T) {
	authUseCase := authenticationservice.NewAuthTokenUseCase("", "https://www.my-organization/logo.svg", nil)
	assert.Equal(t, "https://www.my-organization/logo.svg", authUseCase.OrganizationLogoURL())
}
