// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package authenticationservice

type SchuldenRequest struct {
	Id            string
	BSN           string
	Organizations []*Organization
}
