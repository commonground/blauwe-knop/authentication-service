// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package authenticationservice

type SessionToken string
