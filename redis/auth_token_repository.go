// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package redis

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/go-redis/redis/v7"

	"authenticationservice"
)

type RedisRepository struct {
	client *redis.Client
}

func NewAuthTokenRepository(redisDSN string) (*RedisRepository, error) {
	options, err := redis.ParseURL(redisDSN)
	if err != nil {
		return nil, err
	}

	client := redis.NewClient(options)

	_, err = client.Ping().Result()
	if err != nil {
		return nil, err
	}
	rand.Seed(time.Now().UnixNano())
	return &RedisRepository{
		client: client,
	}, nil
}

func (r *RedisRepository) AddBSNToAuthToken(authToken string, bsn *authenticationservice.BSN) error {
	_, err := r.client.Set(fmt.Sprintf("auth-token-%s", authToken), bsn.String(), 0).Result()

	return err
}

func (r *RedisRepository) DoesAuthTokenExist(authToken string) (bool, error) {
	_, err := r.client.Get(fmt.Sprintf("auth-token-%s", authToken)).Result()
	if err != nil {
		if err == redis.Nil {
			return false, nil
		}

		return false, err
	}

	return true, nil
}

func (r *RedisRepository) GetLinkToken(debtRequestID string) (string, error) {
	linkToken, err := r.client.Get(fmt.Sprintf("link-token-%s", debtRequestID)).Result()
	if err != nil {
		if err == redis.Nil {
			return "", nil
		}

		return "", fmt.Errorf("failed to fetch link token: %s", err)
	}

	return linkToken, nil
}

func (r *RedisRepository) CreateLinkToken(linkToken string, requestID string) error {
	_, err := r.client.Set(fmt.Sprintf("link-token-for-request-id-%s", requestID), linkToken, 0).Result()
	if err != nil {
		fmt.Printf("failed to assign request id to request id: %v", err)
		return err
	}

	return nil
}
