# Use go 1.x based on alpine image.
FROM golang:1.19.4-alpine AS build

ADD . /go/src/authenticationservice/
ENV GO111MODULE on
WORKDIR /go/src/authenticationservice
RUN go mod download
RUN go build -o dist/bin/authentication-service ./cmd/authentication-service

# Release binary on latest alpine image.
FROM alpine:latest

COPY --from=build /go/src/authenticationservice/dist/bin/authentication-service /usr/local/bin/authentication-service
COPY --from=build /go/src/authenticationservice/html/templates/home.html /html/templates/home.html

ENV HTML_TEMPLATE_DIRECTORY "/html/templates"

# Add non-priveleged user
RUN adduser -D -u 1001 appuser
USER appuser
CMD ["/usr/local/bin/authentication-service"]
