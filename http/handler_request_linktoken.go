// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"log"
	"net/http"

	"github.com/go-chi/render"

	"authenticationservice"
)

func handlerRequestLinkToken(w http.ResponseWriter, req *http.Request) {
	ctx := req.Context()
	linkTokenUseCase, _ := ctx.Value(linkTokenUseCaseKey).(*authenticationservice.LinkTokenUseCase)

	debtRequestID := req.URL.Query().Get("debtRequestId")
	oin := req.URL.Query().Get("oin")
	sessionToken := req.URL.Query().Get("sessionToken")
	linkToken, err := linkTokenUseCase.RequestLinkToken(oin, debtRequestID, sessionToken)

	if err == authenticationservice.ErrDebtRequestIdEmpty {
		http.Error(w, "debt request ID required", http.StatusBadRequest)
		return
	} else if err == authenticationservice.ErrOINEmpty {
		http.Error(w, "OIN is required", http.StatusBadRequest)
		return
	} else if err == authenticationservice.ErrSessionTokenEmpty {
		http.Error(w, "sessionToken is required", http.StatusBadRequest)
		return
	} else if err == authenticationservice.ErrSessionTokenInvalid {
		http.Error(w, "session invalid", http.StatusUnauthorized)
		return
	} else if err != nil {
		log.Printf("error requesting link token: %s", err)
		http.Error(w, "error processing request", http.StatusInternalServerError)
		return
	}

	render.PlainText(w, req, string(linkToken))
}
