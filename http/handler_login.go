// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"fmt"
	"log"
	"net/http"

	"github.com/go-chi/render"

	"authenticationservice"
)

func handlerLogin(w http.ResponseWriter, req *http.Request) {
	ctx := req.Context()
	authUseCase, _ := ctx.Value(authTokenUseCaseKey).(*authenticationservice.AuthTokenUseCase)

	err := req.ParseForm()
	if err != nil {
		log.Printf("error parsing form: %s", err)
		http.Error(w, "error parsing form", http.StatusBadRequest)
		return
	}

	bsn := req.Form.Get("bsn")
	authToken := req.Form.Get("authtoken")

	err = authUseCase.AddBSNToAuthtoken(authToken, bsn)

	if err == authenticationservice.ErrAuthtokenEmpty {
		http.Error(w, "authtoken required", http.StatusBadRequest)
		return
	} else if err == authenticationservice.ErrBSNEmpty {
		http.Error(w, "bsn required", http.StatusBadRequest)
		return
	} else if err == authenticationservice.ErrBSNInvalid {
		http.Error(w, "the provided bsn is invalid", http.StatusBadRequest)
		return
	} else if err == authenticationservice.ErrAuthTokenDoesNotExist {
		http.Error(w, "authtoken unknown", http.StatusForbidden)
		return
	} else if err != nil {
		log.Printf("error adding BSN to authtoken: %s", err)
		http.Error(w, "error processing request", http.StatusInternalServerError)
		return
	}

	render.PlainText(w, req, fmt.Sprintf("blauweknop.app.login://login?organization=%s&success=true", authUseCase.Organization()))
}
