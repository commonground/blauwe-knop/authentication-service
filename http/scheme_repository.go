// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"encoding/json"
	"fmt"
	"net/http"

	"authenticationservice"
)

type schemeResponseModel []*struct {
	OIN                                 string `json:"oin"`
	Name                                string `json:"name"`
	APIBaseURL                          string `json:"apiBaseUrl"`
	LoginURL                            string `json:"loginUrl"`
	AppLinkProcessURL                   string `json:"appLinkProcessUrl"`
	IsRegistrator                       bool   `json:"isRegistrator"`
	RegistratorURL                      string `json:"registratorUrl"`
	DebtRequestRegisterURL              string `json:"debtRequestRegisterUrl"`
	DebtRequestRegisterOrganizationName string `json:"debtRequestRegisterOrganizationName"`
	DebtRequestRegisterServiceName      string `json:"debtRequestRegisterServiceName"`
}

type SchemeRepository struct {
	baseURL string
}

func NewSchemeRepository(baseURL string) *SchemeRepository {
	return &SchemeRepository{
		baseURL: baseURL,
	}
}

func (s *SchemeRepository) GetOrganizationByOIN(oin string) (*authenticationservice.Organization, error) {
	resp, err := http.Get(s.baseURL)
	if err != nil {
		return nil, fmt.Errorf("failed to fetch scheme: %v", err)
	}

	schemeResponse := &schemeResponseModel{}
	err = json.NewDecoder(resp.Body).Decode(schemeResponse)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	organizations := mapSchemeResponseToModels(schemeResponse)

	for _, organization := range organizations {
		if organization.OIN == oin {
			return organization, nil
		}
	}

	return nil, nil
}

func mapSchemeResponseToModels(schemeResponse *schemeResponseModel) []*authenticationservice.Organization {
	var result []*authenticationservice.Organization

	for _, organization := range *schemeResponse {
		result = append(result, &authenticationservice.Organization{
			OIN:                                 organization.OIN,
			Name:                                organization.Name,
			APIBaseURL:                          organization.APIBaseURL,
			LoginURL:                            organization.LoginURL,
			RegistratorURL:                      organization.RegistratorURL,
			DebtRequestRegisterURL:              organization.DebtRequestRegisterURL,
			DebtRequestRegisterOrganizationName: organization.DebtRequestRegisterOrganizationName,
			DebtRequestRegisterServiceName:      organization.DebtRequestRegisterServiceName,
		})
	}

	return result
}
