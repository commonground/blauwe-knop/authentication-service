// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http_test

import (
	"io/ioutil"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	"authenticationservice"
	http_infra "authenticationservice/http"
)

func Test_CreateRouter_Home(t *testing.T) {
	type fields struct {
		htmlTemplateDirectory string
		linkTokenUseCase      *authenticationservice.LinkTokenUseCase
		authTokenUseCase      *authenticationservice.AuthTokenUseCase
	}
	type args struct {
		sessionToken string
	}
	tests := []struct {
		name                   string
		fields                 fields
		args                   args
		expectedHTTPStatusCode int
		expectedBody           string
	}{
		{
			"happy flow",
			fields{
				htmlTemplateDirectory: "../html/templates",
				authTokenUseCase: func() *authenticationservice.AuthTokenUseCase {
					authTokenRepository := generateAuthTokenRepository(t)
					return authenticationservice.NewAuthTokenUseCase(
						"demo-org",
						"https://image.com/dummy.png",
						authTokenRepository,
					)
				}(),
				linkTokenUseCase: func() *authenticationservice.LinkTokenUseCase {
					linkTokenRepository := generateLinkTokenRepository(t)
					debtRequestRepository := generateDebtRequestRepository(t)
					schemeRepository := generateSchemeRepository(t)
					sessionRegisterRepository := generateSessionRegisterRepository(t)
					return authenticationservice.NewLinkTokenUseCase(
						"0000000000",
						linkTokenRepository,
						debtRequestRepository,
						schemeRepository,
						sessionRegisterRepository,
					)
				}(),
			},
			args{
				sessionToken: "",
			},
			200,
			"<!DOCTYPE html>\n<html>\n<head>\n    <meta charset=\"utf-8\" />\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />\n    <title>Login</title>\n\n    <link href=\"https://fonts.googleapis.com/css?family=Open+Sans:400,600\" rel=\"stylesheet\" type=\"text/css\">\n\n    <style>\n        html,\n        body {\n            font-size: 16px;\n            line-height: 1.5rem;\n            font-family: 'Open Sans', sans-serif;\n            color: #212121;\n            font-weight: 400;\n            padding: 0;\n            margin: 0;\n        }\n\n        .container {\n            padding: 0 1rem 1rem 1rem;\n        }\n\n        h1 {\n            font-size: 1.125rem;\n            font-weight: 600;\n        }\n\n        p {\n            font-size: 1rem;\n        }\n\n        label {\n            cursor: pointer;\n            border-radius: 5px;\n            background-color: #EFEFEF;\n            border: 1px solid #767676;\n            display: block;\n            margin-bottom: .5rem;\n            line-height: 2.75rem;\n        }\n\n        label input {\n            margin: 0 15px 0 15px;\n        }\n\n        button,\n        a {\n            text-decoration: none;\n            display: block;\n            cursor: pointer;\n            border: 0 none;\n            border-radius: 5px;\n            color: #ffffff;\n            text-align: center;\n            line-height: 2.75rem;\n            width: 100%;\n            max-width: 100%;\n            font-size: 1rem;\n            font-family: 'Open Sans', sans-serif;\n        }\n\n        button[type=\"submit\"] {\n            background-color: #dd6200;\n            background-image: url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAxNTAgMTUwIj4KICAgIDxwYXRoIGQ9Ik0xMzYgMTUwSDE0Yy04IDAtMTQtNi0xNC0xNFYxNEMwIDYgNiAwIDE0IDBoMTIyYzggMCAxNCA2IDE0IDE0djEyMmMwIDgtNiAxNC0xNCAxNHoiLz4KICAgIDxwYXRoIGQ9Ik0xNyAxMTVWNzloMTBjMTIgMCAxOSA2IDE5IDE3IDAgMTMtOCAxOS0xOSAxOUgxN3ptNi02aDRjNyAwIDEyLTQgMTItMTMgMC04LTUtMTItMTMtMTJoLTN2MjV6TTU0IDc3YzMgMCA0IDEgNCAzcy0xIDQtNCA0Yy0yIDAtMy0yLTMtNHMxLTMgMy0zem0zIDM4aC02Vjg4aDZ2Mjd6TTcyIDEwOWg2YzYgMCA5IDMgOSA3IDAgNS00IDktMTQgOS04IDAtMTEtMi0xMS03IDAtMiAxLTQgNC02bC0yLTNjMC0yIDEtMyAzLTQtMy0yLTQtNC00LTggMC02IDQtMTAgMTEtMTBsNCAxaDl2NGgtNGwyIDVjMCA2LTQgOS0xMiA5aC0zbC0xIDFjMCAyIDEgMiAzIDJ6bTEgMTJjNiAwIDgtMiA4LTRzLTEtMi0zLTJsLTktMS0yIDNjMCAyIDIgNCA2IDR6bTYtMjRjMC0zLTItNS01LTVzLTUgMS01IDUgMSA1IDUgNWMzIDAgNS0xIDUtNXoiIGZpbGw9IiNmZmYiLz4KICAgIDxwYXRoIGQ9Ik05NCA3N2MyIDAgMyAxIDMgM3MtMSA0LTMgNGMtMyAwLTQtMi00LTRzMS0zIDQtM3ptMyAzOGgtNlY4OGg2djI3ek0xMDUgMTE1Vjc5aDEwYzEyIDAgMTggNiAxOCAxNyAwIDEzLTcgMTktMTkgMTloLTl6bTYtNmg0YzcgMCAxMi00IDEyLTEzIDAtOC01LTEyLTEzLTEyaC0zdjI1eiIgZmlsbD0iI0UxNzAwMCIvPgo8L3N2Zz4K');\n            background-repeat: no-repeat;\n            background-size: 2.2rem;\n            background-position: 5px;\n        }\n\n        a {\n            background-color: #063D63;\n            background-image: url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNCIgaGVpZ2h0PSIyNCI+PHBhdGggZmlsbD0iI2ZmZmZmZiIgZD0iTTUgM2MtMS4wOTMgMC0yIC45MDctMiAydjE0YzAgMS4wOTMuOTA3IDIgMiAyaDE0YzEuMDkzIDAgMi0uOTA3IDItMnYtN2gtMnY3SDVWNWg3VjNINXptOSAwdjJoMy41ODZsLTkuMjkzIDkuMjkzIDEuNDE0IDEuNDE0TDE5IDYuNDE0VjEwaDJWM2gtN3oiLz48L3N2Zz4=');\n            background-repeat: no-repeat;\n            background-position: right 10px center;\n        }\n\n        .hidden {\n            display: none;\n        }\n\n        div.check {\n            display: block;\n            width: 100%;\n            height: 64px;\n            background-image: url('data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNDQiIGhlaWdodD0iNDQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj48Y2lyY2xlIGZpbGw9IiMzOTg3MEMiIGN4PSIyMiIgY3k9IjIyIiByPSIyMiIvPjxwYXRoIGQ9Ik0xOSAyOGwxMi45OTktMTNMMzQgMTcgMTkgMzJsLTktOSAyLTJ6IiBmaWxsPSIjRkZGIiBmaWxsLXJ1bGU9Im5vbnplcm8iLz48L2c+PC9zdmc+');\n            background-position: center;\n            background-repeat: no-repeat;\n        }\n    </style>\n</head>\n<body>\n\n    <img src=\"https://image.com/dummy.png\" style=\"max-height: 75px;max-width: 100%;\">\n    <div class=\"container\">\n        <h1>demo-org</h1>\n\n        <div id=\"login-page\">\n            <p>Met welk account wil je inloggen?</p>\n\n            <form action=\"/auth/login\" method=\"POST\">\n                <input id=\"authtoken\" type=\"hidden\" name=\"authtoken\"/>\n                <label><input type=\"radio\" value=\"814859094\" name=\"bsn\"/>Piet Verstraten</label>\n                <label><input type=\"radio\" value=\"309777938\" name=\"bsn\"/>Barbara Goulouse</label>\n                <label><input type=\"radio\" value=\"950053545\" name=\"bsn\"/>Çigdem Kemal</label>\n                <label><input type=\"radio\" value=\"761582915\" name=\"bsn\"/>Hendrik-jan Jansens</label>\n                <label><input type=\"radio\" value=\"570957588\" name=\"bsn\"/>Lance Smit</label>\n                <br>\n                <button type=\"submit\">Inloggen</button>\n            </form>\n        </div>\n\n        <div id=\"success-page\" class=\"hidden\">\n            <div class=\"check\"></div>\n\n            <p style=\"text-align: center;font-weight: 600;\">\n                Inloggen gelukt\n            </p>\n            <p>\n                Het inloggen is gelukt. Je kan nu terug naar de Blauwe Knop app, om je schuldenoverzicht op te halen.\n            </p>\n\n            <a href=\"#\">Ga verder in de Blauwe Knop app</a>\n\n        </div>\n    </div>\n\n    <script>\n        var urlParams = new URLSearchParams(window.location.search);\n        var authToken = urlParams.get('authtoken');\n        var authTokenInput = document.querySelector('input[name=\"authtoken\"]')\n        authTokenInput.value = authToken\n\n\n        var form = document.querySelector('form')\n        var submitLoginUrl = form.getAttribute('action')\n\n        form.addEventListener('submit', (event) => {\n          event.preventDefault()\n\n          var formData = new FormData(form)\n          fetch(submitLoginUrl, {\n            method: 'POST',\n            redirect: 'manual',\n            mode: 'no-cors',\n            headers: {\n              'Content-Type': 'application/x-www-form-urlencoded',\n            },\n            body: new URLSearchParams(formData)\n          })\n          .then(response => response.text())\n          .then(redirectUrl => {\n            var anchor = document.querySelector('a')\n            anchor.setAttribute('href', redirectUrl)\n\n            var loginPage = document.getElementById('login-page').classList.toggle('hidden')\n            var successPage = document.getElementById('success-page').classList.toggle('hidden')\n          })\n        })\n\n    </script>\n</body>\n</html>\n",
		},
		{
			"invalid template",
			fields{
				htmlTemplateDirectory: "../testing/invalid-html-templates",
				authTokenUseCase: func() *authenticationservice.AuthTokenUseCase {
					authTokenRepository := generateAuthTokenRepository(t)
					return authenticationservice.NewAuthTokenUseCase(
						"demo-org",
						"https://image.com/dummy.png",
						authTokenRepository,
					)
				}(),
				linkTokenUseCase: func() *authenticationservice.LinkTokenUseCase {
					linkTokenRepository := generateLinkTokenRepository(t)
					debtRequestRepository := generateDebtRequestRepository(t)
					schemeRepository := generateSchemeRepository(t)
					sessionRegisterRepository := generateSessionRegisterRepository(t)
					return authenticationservice.NewLinkTokenUseCase(
						"0000000000",
						linkTokenRepository,
						debtRequestRepository,
						schemeRepository,
						sessionRegisterRepository,
					)
				}(),
			},
			args{
				sessionToken: "",
			},
			500,
			"the page could not be loaded\n",
		},
		{
			"non-existent template",
			fields{
				htmlTemplateDirectory: "../directory-which-does-not-exist",
				authTokenUseCase: func() *authenticationservice.AuthTokenUseCase {
					authTokenRepository := generateAuthTokenRepository(t)
					return authenticationservice.NewAuthTokenUseCase(
						"demo-org",
						"https://image.com/dummy.png",
						authTokenRepository,
					)
				}(),
				linkTokenUseCase: func() *authenticationservice.LinkTokenUseCase {
					linkTokenRepository := generateLinkTokenRepository(t)
					debtRequestRepository := generateDebtRequestRepository(t)
					schemeRepository := generateSchemeRepository(t)
					sessionRegisterRepository := generateSessionRegisterRepository(t)
					return authenticationservice.NewLinkTokenUseCase(
						"0000000000",
						linkTokenRepository,
						debtRequestRepository,
						schemeRepository,
						sessionRegisterRepository,
					)
				}(),
			},
			args{
				sessionToken: "",
			},
			500,
			"the page could not be loaded\n",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			router := http_infra.NewRouter(
				test.fields.htmlTemplateDirectory,
				test.fields.authTokenUseCase,
				test.fields.linkTokenUseCase,
			)
			w := httptest.NewRecorder()

			request := httptest.NewRequest("GET", "/auth", nil)
			request.Header.Add("Authorization", test.args.sessionToken)

			router.ServeHTTP(w, request)

			resp := w.Result()
			body, _ := ioutil.ReadAll(resp.Body)

			assert.Equal(t, test.expectedHTTPStatusCode, resp.StatusCode)
			assert.Equal(t, test.expectedBody, string(body))
		})
	}
}
