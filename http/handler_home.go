// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"html/template"
	"log"
	"net/http"
	"path"
	"path/filepath"
)

type organization struct {
	Name    string
	LogoURL string
}

func handlerHome(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	templateDirectory, _ := ctx.Value(htmlTemplateDirectoryKey).(string)
	organizationName, _ := ctx.Value(organizationNameKey).(string)
	organizationLogoURL, _ := ctx.Value(organizationLogoURLKey).(string)

	templatePath := filepath.Join(templateDirectory, "home.html")
	filePath := path.Join(templatePath)

	t, err := template.ParseFiles(filePath)
	if err != nil {
		log.Printf("error parsing template: %s", err)
		http.Error(w, "the page could not be loaded", http.StatusInternalServerError)
		return
	}

	err = t.Execute(w, &organization{
		Name:    organizationName,
		LogoURL: organizationLogoURL,
	})

	if err != nil {
		log.Printf("error serving template: %s", err)
		http.Error(w, "the page could not be loaded", http.StatusInternalServerError)
		return
	}
}
