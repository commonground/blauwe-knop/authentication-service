// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"authenticationservice"
)

type SessionRegisterRepositoryEndpoint struct {
	baseURL string
	apiKey  string
}

type sessionTokenModel struct {
	SessionToken string `json:"sessionToken"`
}

func NewSessionRegisterRepository(baseURL string, apiKey string) *SessionRegisterRepositoryEndpoint {
	return &SessionRegisterRepositoryEndpoint{
		baseURL: baseURL,
		apiKey:  apiKey,
	}
}

func (s *SessionRegisterRepositoryEndpoint) GetSession(sessionToken string) (*authenticationservice.SessionToken, error) {
	requestBody := sessionTokenModel{sessionToken}
	requestBodyAsJson, err := json.Marshal(requestBody)
	if err != nil {
		return nil, fmt.Errorf("failed to marshall request body: %v", err)
	}

	url := fmt.Sprintf("%s/sessions/", s.baseURL)
	req, err := http.NewRequest(http.MethodGet, url, bytes.NewBuffer(requestBodyAsJson))
	if err != nil {
		return nil, fmt.Errorf("failed to get session request: %v", err)
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authentication", s.apiKey)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("failed to get session: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while retrieving session: %d", resp.StatusCode)
	}

	sessionResponse := &sessionTokenModel{}
	err = json.NewDecoder(resp.Body).Decode(sessionResponse)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	log.Printf("SessionToken: %s", sessionResponse.SessionToken)

	return (*authenticationservice.SessionToken)(&sessionResponse.SessionToken), nil
}
