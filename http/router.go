// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"context"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"

	"authenticationservice"
)

func NewRouter(htmlTemplateDirectory string, authTokenUseCase *authenticationservice.AuthTokenUseCase, linkTokenUseCase *authenticationservice.LinkTokenUseCase) *chi.Mux {
	r := chi.NewRouter()

	r.Use(middleware.Logger)

	r.Route("/auth", func(r chi.Router) {
		r.Get("/", func(w http.ResponseWriter, r *http.Request) {
			ctx := context.WithValue(r.Context(), htmlTemplateDirectoryKey, htmlTemplateDirectory)
			ctx = context.WithValue(ctx, organizationNameKey, authTokenUseCase.Organization())
			ctx = context.WithValue(ctx, organizationLogoURLKey, authTokenUseCase.OrganizationLogoURL())
			handlerHome(w, r.WithContext(ctx))
		})
		r.Post("/login", func(w http.ResponseWriter, r *http.Request) {
			ctx := context.WithValue(r.Context(), authTokenUseCaseKey, authTokenUseCase)
			handlerLogin(w, r.WithContext(ctx))
		})
		r.Get("/request-link-token", func(w http.ResponseWriter, r *http.Request) {
			ctx := context.WithValue(r.Context(), linkTokenUseCaseKey, linkTokenUseCase)
			handlerRequestLinkToken(w, r.WithContext(ctx))
		})
	})

	return r
}

type key int

const (
	authTokenUseCaseKey      key = iota
	linkTokenUseCaseKey      key = iota
	htmlTemplateDirectoryKey key = iota
	organizationNameKey      key = iota
	organizationLogoURLKey   key = iota
)
