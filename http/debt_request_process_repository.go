// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"authenticationservice"
)

type organizationModel struct {
	OIN                                 string `json:"oin"`
	Name                                string `json:"name"`
	APIBaseURL                          string `json:"apiBaseUrl"`
	LoginURL                            string `json:"loginUrl"`
	AppLinkProcessURL                   string `json:"appLinkProcessUrl"`
	IsRegistrator                       bool   `json:"isRegistrator"`
	RegistratorURL                      string `json:"registratorUrl"`
	DebtRequestRegisterURL              string `json:"debtRequestRegisterUrl"`
	DebtRequestRegisterOrganizationName string `json:"debtRequestRegisterOrganizationName"`
	DebtRequestRegisterServiceName      string `json:"debtRequestRegisterServiceName"`
}

type debtRequestResponseModel struct {
	Id            string               `json:"id"`
	BSN           string               `json:"bsn"`
	Organizations []*organizationModel `json:"organizations"`
}

type DebtRequestRegisterRepository struct {
	outwayAddress string
}

func NewDebtRequestRegisterRepository(outwayAddress string) *DebtRequestRegisterRepository {
	return &DebtRequestRegisterRepository{
		outwayAddress: outwayAddress,
	}
}

func (s *DebtRequestRegisterRepository) Get(organization *authenticationservice.Organization, id string) (*authenticationservice.SchuldenRequest, error) {
	var resp *http.Response
	var err error

	path := fmt.Sprintf("debt-requests/%s", id)

	log.Printf("path: %s, outway address: %s", path, s.outwayAddress)
	log.Printf("service name: %s, organization name: %s, organization debt request register url: %s",
		organization.DebtRequestRegisterServiceName, organization.DebtRequestRegisterOrganizationName, organization.DebtRequestRegisterURL)

	if s.outwayAddress == "" {
		log.Printf("No outway address configured, so we use organization.DebtRequestRegisterURL (Third party host)")
		if organization.DebtRequestRegisterURL == "" {
			return nil, fmt.Errorf("outwayAddress or organization.DebtRequestRegisterURL should contain a url")
		}
		resp, err = http.Get(fmt.Sprintf("%s/%s", organization.DebtRequestRegisterURL, path))
		if err != nil {
			return nil, fmt.Errorf("failed to fetch debt request: %v", err)
		}
	} else {
		log.Printf("Outway address configured, ignoring organization.DebtRequestRegisterURL (NLX hosted)")
		outwayUrl := fmt.Sprintf("%s/%s/%s", s.outwayAddress, organization.DebtRequestRegisterOrganizationName, organization.DebtRequestRegisterServiceName)
		resp, err = http.Get(fmt.Sprintf("%s/%s", outwayUrl, path))
		if err != nil {
			return nil, fmt.Errorf("failed to fetch debt request: %v", err)
		}
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while fetching debt request %d", resp.StatusCode)
	}

	debtRequestResponse := &debtRequestResponseModel{}
	err = json.NewDecoder(resp.Body).Decode(debtRequestResponse)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	return mapResponseToModels(debtRequestResponse), nil
}

func mapResponseToModels(debtRequestResponse *debtRequestResponseModel) *authenticationservice.SchuldenRequest {
	result := authenticationservice.SchuldenRequest{}

	result.Id = debtRequestResponse.Id
	result.BSN = debtRequestResponse.BSN
	result.Organizations = []*authenticationservice.Organization{}

	for _, organization := range debtRequestResponse.Organizations {
		result.Organizations = append(result.Organizations, &authenticationservice.Organization{
			OIN:                                 organization.OIN,
			Name:                                organization.Name,
			APIBaseURL:                          organization.APIBaseURL,
			LoginURL:                            organization.LoginURL,
			RegistratorURL:                      organization.RegistratorURL,
			DebtRequestRegisterURL:              organization.DebtRequestRegisterURL,
			DebtRequestRegisterOrganizationName: organization.DebtRequestRegisterOrganizationName,
			DebtRequestRegisterServiceName:      organization.DebtRequestRegisterServiceName,
		})
	}

	return &result
}
