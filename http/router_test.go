// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http_test

import (
	"testing"

	"github.com/golang/mock/gomock"

	"authenticationservice"
	"authenticationservice/mock"
)

var DummyLinkToken = authenticationservice.LinkToken("dummy-link-token")

func generateAuthTokenRepository(t *testing.T) *mock.MockAuthTokenRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	repo := mock.NewMockAuthTokenRepository(ctrl)
	return repo
}

func generateLinkTokenRepository(t *testing.T) *mock.MockLinkTokenRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	repo := mock.NewMockLinkTokenRepository(ctrl)
	return repo
}

func generateDebtRequestRepository(t *testing.T) *mock.MockDebtRequestRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	repo := mock.NewMockDebtRequestRepository(ctrl)
	return repo
}

func generateSchemeRepository(t *testing.T) *mock.MockSchemeRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	repo := mock.NewMockSchemeRepository(ctrl)
	return repo
}

func generateSessionRegisterRepository(t *testing.T) *mock.MockSessionRegisterRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	repo := mock.NewMockSessionRegisterRepository(ctrl)
	return repo
}

func generateMockLinkTokenUseCase(t *testing.T) *authenticationservice.LinkTokenUseCase {
	linkTokenRepository := generateLinkTokenRepository(t)
	debtRequestUseCase := generateDebtRequestRepository(t)
	schemeRepository := generateSchemeRepository(t)
	sessionRegisterRepository := generateSessionRegisterRepository(t)
	return authenticationservice.NewLinkTokenUseCase(DummyOIN, linkTokenRepository, debtRequestUseCase, schemeRepository, sessionRegisterRepository)
}
