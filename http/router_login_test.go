// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http_test

import (
	"errors"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"

	"authenticationservice"
	http_infra "authenticationservice/http"
)

func Test_CreateRouter_Login(t *testing.T) {
	type fields struct {
		authTokenUseCase *authenticationservice.AuthTokenUseCase
		linkTokenUseCase *authenticationservice.LinkTokenUseCase
	}
	type args struct {
		authToken string
		bsn       string
	}
	tests := []struct {
		name                   string
		fields                 fields
		args                   args
		expectedHTTPStatusCode int
		expectedBody           string
	}{
		{
			"without authToken",
			fields{
				authTokenUseCase: func() *authenticationservice.AuthTokenUseCase {
					authTokenRepository := generateAuthTokenRepository(t)
					return authenticationservice.NewAuthTokenUseCase("", "", authTokenRepository)
				}(),
				linkTokenUseCase: generateMockLinkTokenUseCase(t),
			},
			args{
				authToken: "",
				bsn:       "814859094",
			},
			http.StatusBadRequest,
			"authtoken required\n",
		},
		{
			"without bsn",
			fields{
				authTokenUseCase: func() *authenticationservice.AuthTokenUseCase {
					authTokenRepository := generateAuthTokenRepository(t)
					return authenticationservice.NewAuthTokenUseCase("", "", authTokenRepository)
				}(),
				linkTokenUseCase: generateMockLinkTokenUseCase(t),
			},
			args{
				authToken: "authtoken",
				bsn:       "",
			},
			http.StatusBadRequest,
			"bsn required\n",
		},
		{
			"invalid bsn",
			fields{
				authTokenUseCase: func() *authenticationservice.AuthTokenUseCase {
					authTokenRepository := generateAuthTokenRepository(t)
					return authenticationservice.NewAuthTokenUseCase("", "", authTokenRepository)
				}(),
				linkTokenUseCase: generateMockLinkTokenUseCase(t),
			},
			args{
				authToken: "authtoken",
				bsn:       "invalidbsn",
			},
			http.StatusBadRequest,
			"the provided bsn is invalid\n",
		},
		{
			"authtoken does not exist",
			fields{
				authTokenUseCase: func() *authenticationservice.AuthTokenUseCase {
					authTokenRepository := generateAuthTokenRepository(t)
					authTokenRepository.EXPECT().DoesAuthTokenExist("authtoken").Return(false, nil).AnyTimes()
					return authenticationservice.NewAuthTokenUseCase("", "", authTokenRepository)
				}(),
				linkTokenUseCase: generateMockLinkTokenUseCase(t),
			},
			args{
				authToken: "authtoken",
				bsn:       "814859094",
			},
			http.StatusForbidden,
			"authtoken unknown\n",
		},
		{
			"error adding bsn to authtoken",
			fields{
				authTokenUseCase: func() *authenticationservice.AuthTokenUseCase {
					authTokenRepository := generateAuthTokenRepository(t)

					bsn, _ := authenticationservice.NewBSN("814859094")
					authTokenRepository.EXPECT().DoesAuthTokenExist("authtoken").Return(true, nil).AnyTimes()
					authTokenRepository.EXPECT().AddBSNToAuthToken("authtoken", bsn).Return(errors.New("error")).AnyTimes()

					return authenticationservice.NewAuthTokenUseCase("", "", authTokenRepository)
				}(),
				linkTokenUseCase: generateMockLinkTokenUseCase(t),
			},
			args{
				authToken: "authtoken",
				bsn:       "814859094",
			},
			http.StatusInternalServerError,
			"error processing request\n",
		},
		{
			"redirect after successfully adding bsn to authtoken",
			fields{
				authTokenUseCase: func() *authenticationservice.AuthTokenUseCase {
					authTokenRepository := generateAuthTokenRepository(t)

					bsn, _ := authenticationservice.NewBSN("814859094")
					authTokenRepository.EXPECT().DoesAuthTokenExist("authtoken").Return(true, nil).AnyTimes()
					authTokenRepository.EXPECT().AddBSNToAuthToken("authtoken", bsn).Return(nil).AnyTimes()

					return authenticationservice.NewAuthTokenUseCase("demo-org", "", authTokenRepository)
				}(),
				linkTokenUseCase: generateMockLinkTokenUseCase(t),
			},
			args{
				authToken: "authtoken",
				bsn:       "814859094",
			},
			http.StatusOK,
			"blauweknop.app.login://login?organization=demo-org&success=true",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			router := http_infra.NewRouter("", test.fields.authTokenUseCase, test.fields.linkTokenUseCase)
			w := httptest.NewRecorder()

			form := url.Values{}
			form.Add("authtoken", test.args.authToken)
			form.Add("bsn", test.args.bsn)

			request := httptest.NewRequest("POST", "/auth/login", strings.NewReader(form.Encode()))
			request.PostForm = form
			request.Header.Add("Content-Type", "application/x-www-form-urlencoded")

			router.ServeHTTP(w, request)

			resp := w.Result()
			body, _ := ioutil.ReadAll(resp.Body)

			assert.Equal(t, test.expectedHTTPStatusCode, resp.StatusCode)
			assert.Equal(t, test.expectedBody, string(body))
		})
	}
}
