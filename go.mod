module authenticationservice

go 1.19

require (
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/render v1.0.2
	github.com/go-redis/redis/v7 v7.4.1
	github.com/golang/mock v1.6.0
	github.com/stretchr/testify v1.8.1
	github.com/svent/go-flags v0.0.0-20141123140740-4bcbad344f03
	go.uber.org/zap v1.24.0
)

require (
	github.com/ajg/form v1.5.1 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/onsi/ginkgo v1.16.4 // indirect
	github.com/onsi/gomega v1.16.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
