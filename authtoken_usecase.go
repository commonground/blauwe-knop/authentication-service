// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package authenticationservice

import "errors"

type AuthTokenRepository interface {
	AddBSNToAuthToken(authToken string, bsn *BSN) error
	DoesAuthTokenExist(authToken string) (bool, error)
}

var ErrAuthtokenEmpty = errors.New("authtoken is empty")
var ErrBSNEmpty = errors.New("bsn empty")
var ErrBSNInvalid = errors.New("bsn invalid")
var ErrAuthTokenDoesNotExist = errors.New("authtoken does not exist")

type AuthTokenUseCase struct {
	organization        string
	organizationLogoURL string
	repository          AuthTokenRepository
}

func NewAuthTokenUseCase(organization string, organizationLogoURL string, repository AuthTokenRepository) *AuthTokenUseCase {
	return &AuthTokenUseCase{
		organization,
		organizationLogoURL,
		repository,
	}
}

func (a *AuthTokenUseCase) AddBSNToAuthtoken(authToken, bsnString string) error {
	if authToken == "" {
		return ErrAuthtokenEmpty
	}

	if bsnString == "" {
		return ErrBSNEmpty
	}

	bsn, err := NewBSN(bsnString)
	if err != nil {
		return ErrBSNInvalid
	}

	exists, err := a.repository.DoesAuthTokenExist(authToken)
	if err != nil {
		return err
	}

	if !exists {
		return ErrAuthTokenDoesNotExist
	}

	return a.repository.AddBSNToAuthToken(authToken, bsn)
}

func (a *AuthTokenUseCase) Organization() string {
	return a.organization
}

func (a *AuthTokenUseCase) OrganizationLogoURL() string {
	return a.organizationLogoURL
}
