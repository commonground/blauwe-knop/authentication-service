# Authentication service

Authentication service written in golang.

## Installation

Prerequisites:

- [Git](https://git-scm.com/)
- [Golang](https://golang.org/doc/install)
- [Modd](https://github.com/cortesi/modd)
- [Redis](https://redis.io)

1. Download the required Go dependencies:

```sh
go mod download
```

1. Spin up Redis. We've provided instructions on how to do this using Docker Compose below.

1. Now start the authentication service:

```sh
go run cmd/authentication-service/main.go --organization demo-org
```

Or run the authentication service using modd, which wil restart the API on file changes.

```sh
modd
```

By default, the authentication service will run on port `8081`.

### Running Redis

The authorization service uses a Redis database for data storage. You can launch it using Docker Compose.

```sh
docker-compose -f docker-compose.dev.yaml up -d
```

> The `-d` flag makes it run in the background. To stop the services simply type: `docker-compose stop`

## Populate Redis with test data

Instructions for Mac OS.

You will need `redis-cli` to access your local Redis database, you can install it by using `brew`

```sh
brew install redis
```

Start `redis-cli`, it will automatically connect to your local Redis instance

```sh
redis-cli
```

Wile in `redis-cli` execute the following command to add an auth-token for testing purposes

```sh
set auth-token-<your-auth-token> ""
```

## Adding mocks

We use [GoMock](https://github.com/golang/mock) to generate mocks.
When you make updates to code for which there are mocks, you should regenerate the mocks.

**Regenerating mocks**

```sh
sh regenerate-gomock-files.sh
```

## Deployment

Prerequisites:

- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [helm](https://helm.sh/docs/intro/)
- deployment access to the haven cluster `azure-common-prod`

You will need to have a [Redis](https://redis.io/) database running on the cluster.
If you do not have Redis running on the cluster you can use `helm` to install it.

First add the chart to `helm`

```sh
helm repo add bitnami https://charts.bitnami.com/bitnami
```

Now use `helm` to deploy the Redis chart on the cluster

```sh
helm install -n bk-test redis bitnami/redis
```

Once Redis is runnning you can use `helm` to deploy the authentication service

```sh
helm upgrade --install authentication-service ./charts/authentication-service -n bk-test
```
