// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package main

import (
	"fmt"
	"log"
	"net/http"
	"net/url"

	"authenticationservice"
	file_infra "authenticationservice/file"
	http_infra "authenticationservice/http"
	"authenticationservice/redis"
)

type options struct {
	Organization           string `long:"organization" env:"ORGANIZATION" description:"Organization name"`
	OrganizationOIN        string `long:"organization-oin" env:"ORGANIZATION_OIN" description:"Organization OIN"`
	OrganizationLogoURL    string `long:"organization-logo-url" env:"ORGANIZATION_LOGO_URL" description:"Organization logo URL"`
	HtmlTemplateDirectory  string `long:"html-template-directory" env:"HTML_TEMPLATE_DIRECTORY" default:"html/templates/" description:"The directory in which the html templates can be found"`
	ListenAddress          string `long:"listen-address" env:"LISTEN_ADDRESS" default:"0.0.0.0:8081" description:"Address for the authentication-service api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	RedisDSN               string `long:"redis-dsn" env:"REDIS_DSN" default:"redis://0.0.0.0:6379/0" description:"DSN for the redis database. Read https://pkg.go.dev/github.com/go-redis/redis/v8?tab=doc#ParseURL for more info"`
	SchemeURI              string `long:"scheme-uri" env:"SCHEME_URI" default:"https://gitlab.com/commonground/blauwe-knop/scheme/-/raw/master/organizations.json" description:"URI of the scheme containing the organizations. Can be a file path or a URL"`
	OutwayAddress          string `long:"outway-address" env:"OUTWAY_ADDRESS" default:"" description:"Outway address for NLX. Leave empty to omit NLX."`
	LinkRegisterAddress    string `long:"link-register-address" env:"LINK_REGISTER_ADDRESS" default:"http://localhost:8085" description:"Link register address."`
	LinkRegisterAPIKey     string `long:"link-register-api-key" env:"LINK_REGISTER_API_KEY" default:"" description:"API key to use when calling the link-register service."`
	SessionRegisterAddress string `long:"session-register-address" env:"SESSION_REGISTER_ADDRESS" default:"http://localhost:8084" description:"Session register address."`
	SessionRegisterAPIKey  string `long:"session-register-api-key" env:"SESSION_REGISTER_API_KEY" default:"" description:"API key to use when calling the session-register service."`

	LogOptions
}

func main() {
	cliOptions, err := parseOptions()
	if err != nil {
		log.Fatalf("failed to parse options: %s", err)
	}

	logger, err := newZapLogger(cliOptions.LogOptions)
	if err != nil {
		log.Fatalf("failed to create logger: %s", err)
	}

	if cliOptions.Organization == "" {
		log.Fatalf("please specify an organization")
	}

	if cliOptions.OrganizationOIN == "" {
		log.Fatalf("please specify an organization OIN")
	}

	if cliOptions.SchemeURI == "" {
		log.Fatalf("please specify the scheme URI")
	}

	authTokenRepository, err := redis.NewAuthTokenRepository(cliOptions.RedisDSN)
	if err != nil {
		panic(err)
	}

	sessionRegisterRepository := http_infra.NewSessionRegisterRepository(cliOptions.SessionRegisterAddress, cliOptions.SessionRegisterAPIKey)

	debtRequestProcessRepository := http_infra.NewDebtRequestRegisterRepository(cliOptions.OutwayAddress)

	linkRegisterRepository := http_infra.NewLinkRegisterRepository(cliOptions.LinkRegisterAddress, cliOptions.LinkRegisterAPIKey)

	schemeURL, err := url.Parse(cliOptions.SchemeURI)
	if err != nil {
		log.Fatalf("error parsing scheme URI %s", err)
	}

	var schemeRepository authenticationservice.SchemeRepository
	if schemeURL.Scheme == "file" {
		schemeRepository, err = file_infra.NewSchemeRepository(schemeURL.Hostname())
		if err != nil {
			log.Fatalf("error create file scheme repository: %s", err)
		}
	} else {
		schemeRepository = http_infra.NewSchemeRepository(cliOptions.SchemeURI)
	}

	authTokenUseCase := authenticationservice.NewAuthTokenUseCase(cliOptions.Organization, cliOptions.OrganizationLogoURL, authTokenRepository)
	linkTokenUseCase := authenticationservice.NewLinkTokenUseCase(cliOptions.OrganizationOIN, linkRegisterRepository, debtRequestProcessRepository, schemeRepository, sessionRegisterRepository)
	router := http_infra.NewRouter(cliOptions.HtmlTemplateDirectory, authTokenUseCase, linkTokenUseCase)

	logger.Info(fmt.Sprintf("start listening on %s", cliOptions.ListenAddress))

	logger.Info(fmt.Sprintf("api key: %s", cliOptions.LinkRegisterAPIKey))

	err = http.ListenAndServe(cliOptions.ListenAddress, router)
	if err != nil {
		if err != http.ErrServerClosed {
			panic(err)
		}
	}
}
