// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package file

import (
	"encoding/json"
	"fmt"
	"io/ioutil"

	"authenticationservice"
)

type SchemeRepository struct {
	organizations []*authenticationservice.Organization
}

func NewSchemeRepository(filePath string) (*SchemeRepository, error) {
	organizations, err := readScheme(filePath)
	if err != nil {
		return nil, fmt.Errorf("error reading scheme: %s", err)
	}
	return &SchemeRepository{
		organizations: organizations,
	}, nil
}

func readScheme(filePath string) ([]*authenticationservice.Organization, error) {
	fileBytes, err := ioutil.ReadFile(filePath)
	if err != nil {
		return nil, fmt.Errorf("failed to read file: %s", err)
	}

	var organizations = []*authenticationservice.Organization{}
	err = json.Unmarshal(fileBytes, &organizations)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal json: %s", err)
	}

	return organizations, nil
}

func (s *SchemeRepository) GetOrganizationByOIN(oin string) (*authenticationservice.Organization, error) {
	for _, organization := range s.organizations {
		if organization.OIN == oin {
			return organization, nil
		}
	}

	return nil, nil
}
